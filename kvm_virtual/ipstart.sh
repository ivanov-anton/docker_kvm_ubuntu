#!/usr/bin/env bash
sleep 10
iptables -t nat -A PREROUTING --dst 172.23.0.2 -p tcp --dport 3389 -j DNAT --to-destination 192.168.122.63
iptables -I FORWARD 1 -i eth0 -o virbr0 -d 192.168.122.63 -p tcp -m tcp --dport 3389 -j ACCEPT