#!/usr/bin/env bash

service libvirtd start
sleep 60
tunctl -t tap11
brctl addif virbr0 tap11
ip link set tap11 up
cd  /src/app
./ipstart.sh &
qemu-system-x86_64 -hda win.qcow -m 16040 -enable-kvm -net nic,vlan=0,macaddr=00:e0:4c:4f:35:82,model=rtl8139 -net tap,vlan=0,ifname=tap11,script=no -nographic

